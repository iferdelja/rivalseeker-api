import cherrypy
import time
import json
import threading
import logging
import sys

def log(msg):
    logging.debug(" ")
    logging.debug("RS: " + msg)

def game_reset():
    global game
    game = Game()

def game_acceptance_check():
    global game
    game.do_acceptance_check()

def load_player_from_json(dct):
    if (dct == None):
        return None
    pid = dct['id']
    if (pid == None):
        return None
    return Player(pid)

class Player:
    def __init__(self,playerid):
        self.id = playerid
        self.did_accept = False

    def json(self):
        return "{ \"id\":\"" + self.id + "\"}"

    def __hash__(self):
        return self.id.__hash__()

    def __eq__(self, other):
        if isinstance(other, Player):
            return self.id == other.id
        return NotImplemented


# Game state machine
# "waiting_players_to_join" - Waiting for all players to invoke joingame api.
#                             Once max players have joined, next state is reached.
#                             Players joins a game by invoking /joingame api
#                             Player may leave a game by invoking /leavegame api
#                             (if invoked in this state, game state is unchanged and player is removed)
#
# "waiting_players_to_accept" - Max players have joined the game and each player must
#                               at this point accept to participate in the game.
#                               Acceptance is required for several reasons: 
#                               (1) give the player a chance to skip a game
#                               (2) prevent players from waiting on a player that might never show up, etc.
#                               Players accepts a game by invoking /acceptgame api
#                               API contract requires that user accepts the game within a time period of 15 seconds.
#                               API will ensure that game is cancelled if all users did not accept the game within
#                               15 seconds + 2 * client_refresh_time, from the moment when this game state was reached. 
#                               If a player leaves the game in this state (via /leavegame), game is cancelled.
#
# "cancelled" - Game was cancelled due to all players not accepting the game.
#               API will ensure game is cleared within 15 seconds from being cancelled.
#
# "ready" - Game is ready if all the players accepted the game (via /acceptgame)
#           API will ensure game is cleared within 15 seconds from being set as ready.
#
class Game:
    def __init__(self):
        self.state = "waiting_players_to_join"
        self.id = str(int(time.time()))
        self.players = set()
        log("New game, id: " + self.id)

    def do_acceptance_check(self):
        log("AC acceptance check triggered")
        # If we are still in accept state - not everybody did accept
        if (self.state == "waiting_players_to_accept"):
            # Look for player that accepted the game.
            # If there is such a player, leave this player in the game since his opponent cancelled on him
            # In this case, we dont reset the game.
            player_that_accepted = None
            for pl in self.players:
                if (pl.did_accept == True):
                    player_that_accepted = pl

            # Nobody accepted the game.
            if (player_that_accepted == None):
                log("AC - No players accepted the game. Game will reset.")
                self.state = "cancelled"
                log("STATE: cancelled")
                threading.Timer(game_reset_timeout, game_reset).start()
            else:
                log("AC - One player accepted the game.")
                transfer_player = Player(player_that_accepted.id)
                self.players = set()
                self.state = "waiting_players_to_join"
                log("STATE: waiting_players_to_join")
                self.add_player(transfer_player)
                self.log_players()

        elif (self.state == "ready" or self.state == "cancelled"):
            log("AC - Game is ready or cancelled. Game will reset.")
            threading.Timer(game_reset_timeout, game_reset).start()

    def add_player(self,player):
        if (self.state != "waiting_players_to_join"):
            log("Cant add player to game. Game not in join state.")
            return False
        if (len(self.players) == maxplayers):
            log("Cant add player to game. Max number of players reached.")
            return False
        if (player in self.players):
            log("Cant add player to game. Already in the game.")
            return False
        self.players.add(player)
        if (len(self.players) == maxplayers):
            self.state = "waiting_players_to_accept"
            log("STATE: waiting_players_to_accept")
            threading.Timer(client_accept_interval + client_poll_interval * 2, game_acceptance_check).start()
        return True

    def player_did_accept(self, player):
        if (self.state == "waiting_players_to_accept"):
            # note that this player did accept the game
            for pl in self.players:
                if (pl.id == player.id):
                    log("Player " + pl.id + " did accept game.")
                    pl.did_accept = True

            # check if all players accepted
            all_players_did_accept = True
            for pl in self.players:
                if (pl.did_accept == False):
                    all_players_did_accept = False
            if (all_players_did_accept == True):
                self.state = "ready"
                log("STATE: ready")

    def remove_player(self,player):
        if (self.state == "waiting_players_to_join"):
            self.players.remove(player)
            log("Removed player " + player.id)
        else:
            log("Cannot remove player. Game not in join state.")
        return True

    def log_players(self):
        log("Players:")
        for pl in self.players:
            log(pl.id)

    def json(self):
        strjson = "{ \"state\":\"" + self.state + "\"," + "\"id\":\"" + self.id + "\"}"
        return strjson.encode("UTF-8")


class RivalSeeker:
    def index(self):
        return "RivalSeeker"
    index.exposed = True

    def games(self):
        log("/games")
        cherrypy.response.status = 200
        cherrypy.response.headers["Content-Type"] = "application/json"
        return game.json()

    games.exposed = True


    def joingame(self):
        cherrypy.response.headers["Content-Type"] = "application/json"
        # Nonworking solution1 - json.load(cherrypy.request.body) - breaks with "read takes 2 arguments but 3 provided"
        #                           Happens on dev.fiveminutes.eu cherrypy instance.
        readbody = cherrypy.request.body.read()
        decoded = readbody.decode("UTF-8")
        player = json.loads(decoded, object_hook=load_player_from_json)
        log("/joingame - playerid: " + player.id)
        if (player == None):
            # We should just crash here since this must always work.
            cherrypy.response.status = 500
        elif(game.add_player(player) == True):
            return game.json()
        else:
            cherrypy.response.status = 204

    joingame.exposed = True


    def acceptgame(self):
        readbody = cherrypy.request.body.read()
        decoded = readbody.decode("UTF-8")
        player = json.loads(decoded, object_hook=load_player_from_json)
        log("/acceptgame - playerid: " + player.id)
        game.player_did_accept(player)

        cherrypy.response.status = 200
        cherrypy.response.headers["Content-Type"] = "application/json"
        return game.json()

    acceptgame.exposed = True


    def leavegame(self):

        readbody = cherrypy.request.body.read()
        decoded = readbody.decode("UTF-8")
        player = json.loads(decoded, object_hook=load_player_from_json)
        log("/leavegame - playerid: " + player.id)
        game.remove_player(player)

        cherrypy.response.status = 200
        cherrypy.response.headers["Content-Type"] = "application/json"
        return game.json()

    leavegame.exposed = True


    def resetgame(self):
        log("/resetgame")
        game = Game()
        cherrypy.response.status = 200
        return "Game reset"
    
    resetgame.exposed = True



cherrypy.config.update({
'environment': 'production',
'log.screen': False,
'server.socket_host': '127.0.0.1',
#'server.socket_host': '0.0.0.0',
'server.socket_port': 31335,
})

#bind to all network interfaces, not just localhost
#cherrypy.config.update({'server.socket_host': '0.0.0.0'})

logging.basicConfig(filename="rivalseeker.log", level=logging.DEBUG)



game = Game()
maxplayers = 2
game_reset_timeout = 15
client_poll_interval = 5
client_accept_interval = 30


cherrypy.quickstart(RivalSeeker())

